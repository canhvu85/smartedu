package com.edu.smartedu.service;

import com.edu.smartedu.model.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    Iterable<User> findAll();
    User findById(Long id);
    User findUserByEmail(String email);
    void save(User user);
    void remove(Long id);

}
