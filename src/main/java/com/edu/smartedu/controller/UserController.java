package com.edu.smartedu.controller;

import com.edu.smartedu.model.Cart;
import com.edu.smartedu.model.Courses;
import com.edu.smartedu.model.User;
import com.edu.smartedu.repository.CartRepository;
import com.edu.smartedu.service.CoursesService;
import com.edu.smartedu.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import static com.edu.smartedu.controller.LoginController.db;

@Controller
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/admin/users")
    public ModelAndView listUsers(){
        if(db != null){
            Iterable<User> users = userService.findAll();
            ModelAndView modelAndView = new ModelAndView("/admin/users");
            modelAndView.addObject("userName", db.getName());
            modelAndView.addObject("users",users);
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView("redirect:/login");
        return modelAndView;
    }


    @Autowired
    CartRepository cartRepository;
    @GetMapping("/admin/users/{id}")
    public ModelAndView courseByUser(@PathVariable Long id){
        User user = userService.findById(id);
       Iterable<Cart> carts = cartRepository.findAllByUser(user);
       ModelAndView modelAndView = new ModelAndView("/admin/course-user");
       modelAndView.addObject("user", user);
       modelAndView.addObject("carts",carts);
       return modelAndView;
    }
}
