package com.edu.smartedu.model;

import org.springframework.web.multipart.MultipartFile;

public class CourseUpload extends Courses {
    private MultipartFile fileDatas;

    public MultipartFile getFileDatas() {
        return fileDatas;
    }

    public void setFileDatas(MultipartFile fileDatas) {
        this.fileDatas = fileDatas;
    }
}
