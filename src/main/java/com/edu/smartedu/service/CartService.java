package com.edu.smartedu.service;

import com.edu.smartedu.model.Cart;
import com.edu.smartedu.model.User;
import org.springframework.stereotype.Service;

@Service
public interface CartService {
    Iterable<Cart> findAll();
    Cart findById(Long id);
    void save(Cart cart);
    void remove(Long id);
}
