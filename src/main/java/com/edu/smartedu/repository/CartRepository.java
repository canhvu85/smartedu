package com.edu.smartedu.repository;

import com.edu.smartedu.model.Cart;
import com.edu.smartedu.model.Courses;
import com.edu.smartedu.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface CartRepository extends JpaRepository<Cart, Long> {
    @Transactional
    @Modifying
    @Query(value = "INSERT INTO carts (course_id, user_id)\n" +
            "VALUES (:idCourse, :idUser)",nativeQuery = true)
    void saveByQuery(@Param("idCourse")Long idCourse,@Param("idUser")Long idUser);

    Iterable<Cart> findAllByUser(User user);

}
