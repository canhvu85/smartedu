package com.edu.smartedu.repository;

import com.edu.smartedu.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;


public interface LoginRepository extends CrudRepository<User, Long> {
    User findUserByEmailAndPassword(String email, String password);
}
