package com.edu.smartedu.controller;

import com.edu.smartedu.model.Courses;
import com.edu.smartedu.model.TypeCourses;
import com.edu.smartedu.service.CoursesService;
import com.edu.smartedu.service.TypeCoursesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

import static com.edu.smartedu.controller.LoginController.db;

@Controller
public class TypeCourseController {
    @Autowired
    CoursesService coursesService;
    @Autowired
    TypeCoursesService typeCoursesService;

    @GetMapping("/create-type")
    public ModelAndView showCreateForm(){
        if(db != null){
            ModelAndView modelAndView = new ModelAndView("/admin/create-type");
            //modelAndView.addObject("typeCourse", new TypeCourses());
            modelAndView.addObject("type", new TypeCourses());
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView("redirect:/login");
        return modelAndView;
    }


    @PostMapping("/create-type")
    // @RequestMapping(value="/create-course" , method= RequestMethod.POST,consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ModelAndView saveBlog(@ModelAttribute("type") TypeCourses typeCourses){
        //coursesService.save(courses);
        typeCoursesService.save(typeCourses);
        ModelAndView modelAndView = new ModelAndView("/admin/create-type");
        modelAndView.addObject("type", new TypeCourses());
        modelAndView.addObject("message", "New type course created successfully");
        return modelAndView;
    }

    @GetMapping("/admin/type-courses")
    public ModelAndView listCourses(){
        if(db != null){
            Iterable<TypeCourses> typeCourses = typeCoursesService.findAll();
            ModelAndView modelAndView = new ModelAndView("/admin/type-courses");
            modelAndView.addObject("userName", db.getName());
            modelAndView.addObject("types", typeCourses);
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView("redirect:/login");
//        Cookie cookie = new Cookie("setUser", setUser);
//        modelAndView.addObject("cookieValue", cookie);

        return modelAndView;
    }

    @GetMapping("/delete-type/{id}")
    public  ModelAndView deleteCourseForm(@PathVariable Long id){
        TypeCourses typeCourses = typeCoursesService.findById(id);
        ModelAndView modelAndView = new ModelAndView("/admin/delete-type");
        modelAndView.addObject("type", typeCourses);
        return modelAndView;
    }

    @PostMapping("/delete-type")
    public ModelAndView deleteBlog(@ModelAttribute("type") TypeCourses typeCourses){
        typeCoursesService.remove(typeCourses.getId());
        Iterable<TypeCourses> typeCourses1 = typeCoursesService.findAll();
        ModelAndView modelAndView = new ModelAndView("/admin/type-courses");
        modelAndView.addObject("types", typeCourses1);
        return modelAndView;
    }

    @GetMapping("/edit-type/{id}")
    public  ModelAndView editCourseForm(@PathVariable Long id){
        TypeCourses typeCourses = typeCoursesService.findById(id);
        ModelAndView modelAndView = new ModelAndView("/admin/edit-type");
        modelAndView.addObject("type",typeCoursesService.findById(id));
       // modelAndView.addObject("course", course);
        return modelAndView;
    }

    @PostMapping("/edit-type")
    public ModelAndView editCourse(@ModelAttribute("type") TypeCourses typeCourses){
        typeCoursesService.save(typeCourses);

        ModelAndView modelAndView = new ModelAndView("/admin/edit-type");
        modelAndView.addObject("type", typeCourses);
        modelAndView.addObject("message", "Type Course edit successfully");
        return modelAndView;
    }

    //for user
    @GetMapping("/courses-type/{id}")
    public ModelAndView coursesType(@PathVariable("id") Long id){
        Iterable<Courses> courses = coursesService.findCoursesByType(id);
        TypeCourses type = typeCoursesService.findById(id);
        ModelAndView modelAndView = new ModelAndView("/courses-type");
        modelAndView.addObject("courses",courses);
        modelAndView.addObject("type", type);
        return modelAndView;
    }

}
