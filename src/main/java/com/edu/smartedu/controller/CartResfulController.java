package com.edu.smartedu.controller;

import com.edu.smartedu.model.Cart;
import com.edu.smartedu.model.Courses;
import com.edu.smartedu.model.User;
import com.edu.smartedu.repository.CartRepository;
import com.edu.smartedu.service.CartService;
import com.edu.smartedu.service.CoursesService;
import com.edu.smartedu.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = {"*"})
@RestController
public class CartResfulController {
    @Autowired
    CartRepository cartRepository;
    @Autowired
    UserService userService;
    @Autowired
    CoursesService coursesService;

    @GetMapping("/carts")
    public List<Cart> getAllCarts() {
        return cartRepository.findAll();
    }

    @PostMapping(value = "/carts", consumes = "application/json")
    public ResponseEntity createCart(@Valid @RequestBody Cart cart) {
        cartRepository.save(cart);

        return ResponseEntity.ok().body(cart);
    }

    @Autowired
    CartService cartService;

    @GetMapping("/carts/{email}/{id}")
    public void createCart(@Valid @PathVariable("email") String email, @PathVariable("id") Long id) {
        User user = userService.findUserByEmail(email);
        Courses courses = coursesService.findById(id);
        int i = courses.getAvailableSeat()-1;
        courses.setAvailableSeat(i);
        cartRepository.saveByQuery(courses.getId(),user.getId());

    }

    @GetMapping("/carts/{id}")
    public Cart getCartById(@PathVariable(value = "id") Long cartId) {
        return cartRepository.findById(cartId).orElse(null);
    }


//-------------------Retrieve All Carts--------------------------------------------------------

//    @RequestMapping(value = "/carts/", method = RequestMethod.GET)
//    public ResponseEntity<List<Cart>> listAllUsers() {
//        List<Cart> carts = (List<Cart>) cartService.findAll();
//        if (carts.isEmpty()) {
//            return new ResponseEntity<List<Cart>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
//        }
//        return new ResponseEntity<List<Cart>>(carts, HttpStatus.OK);
//    }

    //-------------------Retrieve Single Cart--------------------------------------------------------

//    @RequestMapping(value = "/carts/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Cart> getCart(@PathVariable("id") long id) {
//        System.out.println("Fetching Cart with id " + id);
//        Cart cart = cartService.findById(id);
//        if (cart == null) {
//            System.out.println("Cart with id " + id + " not found");
//            return new ResponseEntity<Cart>(HttpStatus.NOT_FOUND);
//        }
//        return new ResponseEntity<Cart>(cart, HttpStatus.OK);
    //}
    //-------------------Create a Cart--------------------------------------------------------

//    @RequestMapping(value = "/carts/", method = RequestMethod.POST)
//    public ResponseEntity<Void> createCart(@RequestBody Cart cart, UriComponentsBuilder ucBuilder) {
//        System.out.println("Creating Cart " + cart.getId());
//        cartService.save(cart);
//        HttpHeaders headers = new HttpHeaders();
//        headers.setLocation(ucBuilder.path("/carts/{id}").buildAndExpand(cart.getId()).toUri());
//        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
//    }
}
