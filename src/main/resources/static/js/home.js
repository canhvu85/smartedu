var course=course || {};
var typeCourse = typeCourse || {};
var user = user || {};
var cart ={};
var coursesbytype = coursesbytype || {};
var typeId;

typeCourse.drawTable = function(){
    $.ajax({
        url:"http://localhost:8081/api/typecourse/",
        method: "GET",
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            $('#typeCourse').html('');
            var response = data;
            $.each(response, function (index, value) {
                var typeCourse = value;
                if (index == 6){
                    $('#typeCourse').append(
                        '<div class="offset-lg-4 col-lg-4 col-md-4 col-sm-6 col-12 text-center mt--100">'
                        +'<a href="/courses-type/'+typeCourse.id+'">'
                        +'<div class="single_department">'
                        +'<div class="dpmt_icon">'
                        +'<img src="'+typeCourse.linkIcon+'" alt="" >'
                        +'</div>'
                        +'<h4>'+typeCourse.name+'</h4>'
                        +'</div>'
                        +'</a>'
                        +'</div>'
                    );
                }else if(index == 4){
                    $('#typeCourse').append(
                        '<div class="col-lg-4 col-md-4 col-sm-6 col-12 text-center mt--100">'
                        +'<a href="/courses-type/'+typeCourse.id+'">'
                        +'<div class="single_department">'
                        +'<div class="dpmt_icon">'
                        +'<img src="'+typeCourse.linkIcon+'" alt="">'
                        +'</div>'
                        +'<h4>'+typeCourse.name+'</h4>'
                        +'</div>'
                        +'</a>'
                        +'</div>'
                    );
                }else if (index % 2 == 0){
                    $('#typeCourse').append(
                        '<div class="col-lg-4 col-md-4 col-sm-6 col-12 text-center mt-100">'
                        +'<a href="/courses-type/'+typeCourse.id+'">'
                        +'<div class="single_department">'
                        +'<div class="dpmt_icon">'
                        +'<img src="'+typeCourse.linkIcon+'" alt="">'
                        +'</div>'
                        +'<h4>'+typeCourse.name+'</h4>'
                        +'</div>'
                        +'</a>'
                        +'</div>'
                    );
                }else {
                    $('#typeCourse').append(
                        '<div class="col-lg-4 col-md-4 col-sm-6 col-12 text-center">'
                        +'<a href="/courses-type/'+typeCourse.id+'">'
                        +'<div class="single_department">'
                        +'<div class="dpmt_icon">'
                        +'<img src="'+typeCourse.linkIcon+'" alt="">'
                        +'</div>'
                        +'<h4>'+typeCourse.name+'</h4>'
                        +'</div>'
                        +'</a>'
                        +'</div>'
                    );
                }
            })
        }
    });
};

course.drawTable = function(){

    $.ajax({
        url:"http://localhost:8081/api/popular-courses/",
        method:"GET",
        dataType:"json",
        contentType:"application/json",
        success: function(data){
            $('#course').html('');
            var response = data;

            $.each(response, function(index, value){
                var course = value;
                var count= course.feedback.length;
                $('#course').append(

                    "<div class='col-lg-3 col-md-6'>"
                    +"<div class='single_course'>"
                    +"<div class='course_head overlay'>"
                    +"<img class='img-fluid w-100' src='/img/courses/"+course.image+"' alt=''>"
                    +"<div class='authr_meta'>"
                    +"<img src='img/author1.png' alt=''>"
                    +"<span>"+course.trainer.name+"</span>"
                    +"</div>"
                    +"</div>"
                    +"<div class='course_content'>"
                    +"<h4>"
                    +"<a href='/course-details/"+course.id+"'>"+course.name+"</a>"
                    +"</h4>"
                    +"<p>"+ course.objectives +"</p>"
                    +"<div class='course_meta d-flex justify-content-between'>"
                    +"<div>"
                    +"<span class='meta_info'>"
                    +"<a href='#'><i class='lnr lnr-user'></i>"+course.availableSeat+"</a>"
                    +"</span>"
                    +"<span class='meta_info'>"
                    +"<a href='#'>"
                    +"<i class='lnr lnr-bubble'></i>"+count
                    +"</a>"
                    +"</span>"
                    +"</div>"
                    +"<div>"
                    +"<span class='price'>"+"$"+course.fee+"</span>"
                    +"</div>"
                    +"</div>"
                    +"</div>"
                    +"</div>"
                    +"</div>"
                );
            })
        }
    });
};



function createUser(name,email,pass){
    $.ajax({
        url: 'http://localhost:8081/users/',
        method: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({name: name , email: email , password: pass}),
        success: function(){

           // alert('Was Created');
            //user.drawTable();
        }
    })
}
// let userTmp;
// function getUser(email){
//     $.ajax({
//         url:"http://localhost:8081/users/",
//         method: "GET",
//         dataType: "json",
//         contentType: "application/json",
//         success: function (data) {
//             var response = data;
//             $.each(response, function (index, value) {
//                 var user = value;
//                 if(email === user.email){
//                     userTmp = user;
//                 }
//             })
//         }
//     });
//     return userTmp;
// }
//
// let tmp;
// function getCourse(id){
//     $.ajax({
//         url:"http://localhost:8081/api/courses/",
//         method:"GET",
//         dataType:"json",
//         contentType:"application/json",
//         success: function(data){
//             var response = data;
//             $.each(response, function(index, value){
//                 var course = value;
//                 if (course.id.toString() === id.toString()){
//                     tmp=course;
//                 }
//
//             })
//         }
//     });
//
//     return tmp;
// }


function createCart(email, courseid){
    $.ajax({
        url: 'http://localhost:8081/carts/'+email+'/'+courseid,
        method: 'GET',
        dataType: 'json',
        contentType: 'application/json',

        success: function(){
            //alert('Was Created');
            //user.drawTable();

        }
    })
}

var emailUser;
var courseId;
user.save = function(){
    var name = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var pass = document.getElementById('pass').value;
    var courseId_ = document.getElementById('courseid').value;
    emailUser = email;
    courseId = courseId_;
    createUser(name, email, pass);
    $('#addEditUser').modal('hide');
    $('#addEditCart').modal('show');
    //createCart(email,courseId);

};

cart.save = function(){
    createCart(emailUser,courseId);
    $('#addEditCart').modal('hide');
};

user.openAddEditUser = function(){
    $('#addEditUser').modal('show');
};

course.init = function(){
    typeCourse.drawTable();
    course.drawTable();
};

$(document).ready(function(){
    course.init();
});