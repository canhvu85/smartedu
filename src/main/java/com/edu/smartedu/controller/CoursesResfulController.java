package com.edu.smartedu.controller;

import com.edu.smartedu.model.Courses;
import com.edu.smartedu.model.TypeCourses;
import com.edu.smartedu.service.CoursesService;
import com.edu.smartedu.service.TypeCoursesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = {"*"})
@RestController
@ComponentScan("com.edu.smartedu")
public class CoursesResfulController {
    @Autowired
    CoursesService coursesService;
    @Autowired
    TypeCoursesService typeCoursesService;

    //-------------------Retrieve All --------------------------------------------------------

    @RequestMapping(value = "/api/courses/", method = RequestMethod.GET)
    public ResponseEntity<List<Courses>> listAllCourses() {
        List<Courses> courses = (List<Courses>) coursesService.findAll();
        if (courses.isEmpty()) {
            return new ResponseEntity<List<Courses>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Courses>>(courses, HttpStatus.OK);
    }

    //-------------------Retrieve All by typecourse --------------------------------------------------------

    @RequestMapping(value = "/api/type/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<Courses>> listAllCoursesType(@Valid @PathVariable("id") Long id) {
        List<Courses> courses = (List<Courses>) coursesService.findCoursesByType(id);
        if (courses.isEmpty()) {
            return new ResponseEntity<List<Courses>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Courses>>(courses, HttpStatus.OK);
    }

    //-------------------Retrieve Popular --------------------------------------------------------

    @RequestMapping(value = "/api/popular-courses/", method = RequestMethod.GET)
    public ResponseEntity<List<Courses>> listPopularCourses() {
        List<Courses> courses = (List<Courses>) coursesService.findPopular();
        if (courses.isEmpty()) {
            return new ResponseEntity<List<Courses>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Courses>>(courses, HttpStatus.OK);
    }




    @RequestMapping(value = "/api/typecourse/", method = RequestMethod.GET)
    public ResponseEntity<List<TypeCourses>> listAllTypeCourse(){
        List<TypeCourses> typeCourses = (List<TypeCourses>) typeCoursesService.findAll();
        if (typeCourses.isEmpty()){
            return new ResponseEntity<List<TypeCourses>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<TypeCourses>>(typeCourses, HttpStatus.OK);
    }

    //-------------------Retrieve Single --------------------------------------------------------

    @RequestMapping(value = "/api/courses/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Courses> singleCourse(@PathVariable("id") Long id) {
        Courses course = coursesService.findById(id);
        if (course == null) {
            return new ResponseEntity<Courses>(HttpStatus.NOT_FOUND);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Courses>(course, HttpStatus.OK);
    }
}
