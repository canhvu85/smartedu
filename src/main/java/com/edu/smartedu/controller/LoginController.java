package com.edu.smartedu.controller;

import com.edu.smartedu.model.User;
import com.edu.smartedu.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@SessionAttributes("user")
@ComponentScan("com.edu.smartedu")
public class LoginController {
    @ModelAttribute("user")
    public User setUpUserForm() {
        return new User();
    }

    @RequestMapping("/login")
    public  String index(@CookieValue(value = "user", defaultValue = "") String setUser, Model model){
        if(db!= null){
            return "redirect:admin/logged";
        }
        Cookie cookie = new Cookie("setUser", setUser);
        model.addAttribute("cookieValue", cookie);
        return "/admin/login";
    }
    @Autowired
    LoginRepository loginRepository;
    public static User db;
    @PostMapping("/dologin")
    public String doLogin(@ModelAttribute("user") User user, Model model, @CookieValue(value = "setUser", defaultValue = "") String setUser,
                          HttpServletResponse response, HttpServletRequest request) {
        //implement business logic
        db = null;
        db = loginRepository.findUserByEmailAndPassword(user.getEmail(),user.getPassword());
        if (db != null && db.getRule()==1) {
            if (user.getEmail() != null)
                setUser = user.getEmail();

            // create cookie and set it in response
            Cookie cookie = new Cookie("setUser", setUser);
            cookie.setMaxAge(24 * 60 * 60);
            response.addCookie(cookie);

            //get all cookies
            Cookie[] cookies = request.getCookies();
            //iterate each cookie
            for (Cookie ck : cookies) {
                //display only the cookie with the name 'setUser'
                if (ck.getName().equals("setUser")) {
                    model.addAttribute("cookieValue", ck);
                    break;
                } else {
                    ck.setValue("");
                    model.addAttribute("cookieValue", ck);
                    break;
                }
            }
            model.addAttribute("message", "Login success. Welcome ");
            return "redirect:admin/logged";

        } else {
            user.setEmail("");
            Cookie cookie = new Cookie("setUser", setUser);
            model.addAttribute("cookieValue", cookie);
            model.addAttribute("message", "Login failed. Try again.");
            return "/admin/login";
        }

    }

//    @GetMapping("/admin/logged")
//    public ModelAndView showCourses(){
//        ModelAndView modelAndView = new ModelAndView("/admin/courses");
//        return modelAndView;
//    }
    @GetMapping("/admin/logged")
    public String showAdminPage(){
        return "redirect:courses";
    }
}
