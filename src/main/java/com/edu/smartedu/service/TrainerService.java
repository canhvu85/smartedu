package com.edu.smartedu.service;

import com.edu.smartedu.model.Trainer;
import com.edu.smartedu.model.TypeCourses;
import org.springframework.stereotype.Service;

@Service
public interface TrainerService {
    Iterable<Trainer> findAll();

    Trainer findById(Long id);

    void save(Trainer trainer);

    void remove(Long id);
}
