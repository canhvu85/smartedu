package com.edu.smartedu.repository;

import com.edu.smartedu.model.Courses;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import javax.transaction.Transactional;


public interface CoursesRepository extends CrudRepository<Courses, Long> {
    Iterable<Courses> findAllByTypeCourses(Long id);

    @Transactional
    @Modifying
    @Query(value = "select * from courses order by available_seat asc limit 0,4\n", nativeQuery = true)
    Iterable<Courses> findPopular();

    @Transactional
    @Modifying
    @Query(value = "select * from courses where type_id=:id ", nativeQuery = true)
    Iterable<Courses> findCoursesByType(@Param("id") Long id);

//    @Transactional
//    @Modifying
//    @Query(value = "select * from courses inner join images on courses.id = images.course_id", nativeQuery = true)
//    Iterable<Courses> findAllWithImage();
}
