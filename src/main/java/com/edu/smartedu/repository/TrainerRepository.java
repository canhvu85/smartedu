package com.edu.smartedu.repository;

import com.edu.smartedu.model.Trainer;
import org.springframework.data.repository.CrudRepository;

public interface TrainerRepository extends CrudRepository<Trainer, Long> {
}
