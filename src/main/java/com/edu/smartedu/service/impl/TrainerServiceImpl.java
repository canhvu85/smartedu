package com.edu.smartedu.service.impl;

import com.edu.smartedu.model.Trainer;
import com.edu.smartedu.repository.TrainerRepository;
import com.edu.smartedu.service.TrainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TrainerServiceImpl implements TrainerService {
    @Autowired
    TrainerRepository trainerRepository;
    @Override
    public Iterable<Trainer> findAll() {
        return trainerRepository.findAll();
    }

    @Override
    public Trainer findById(Long id) {
        return trainerRepository.findById(id).orElse(null);
    }

    @Override
    public void save(Trainer trainer) {
        trainerRepository.save(trainer);
    }

    @Override
    public void remove(Long id) {
        trainerRepository.deleteById(id);
    }
}
