package com.edu.smartedu.repository;

import com.edu.smartedu.model.Image;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ImageRepository extends CrudRepository<Image, Long> {
//    List<String> findAllByCourses_Id(Long id);
//    String findImageByCourses_Id(Long id);
}
