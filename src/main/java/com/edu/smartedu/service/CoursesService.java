package com.edu.smartedu.service;

import com.edu.smartedu.model.Courses;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public interface CoursesService {
    Iterable<Courses> findAll();

    Courses findById(Long id);

//    Iterable<Courses> findAllWithImage();

    Iterable<Courses> findPopular();

    Iterable<Courses> findCoursesByType(Long id);

    void save(Courses courses);

    void remove(Long id);

    void upLoadFile(MultipartFile file) throws IOException;
}
