package com.edu.smartedu.service;

import com.edu.smartedu.model.Courses;
import com.edu.smartedu.model.TypeCourses;
import org.springframework.stereotype.Service;

@Service
public interface TypeCoursesService {
    Iterable<TypeCourses> findAll();

    TypeCourses findById(Long id);

    void save(TypeCourses typeCourses);

    void remove(Long id);
}
