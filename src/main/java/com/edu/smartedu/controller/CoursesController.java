package com.edu.smartedu.controller;

import com.edu.smartedu.model.CourseUpload;
import com.edu.smartedu.model.Courses;
import com.edu.smartedu.model.User;
import com.edu.smartedu.repository.CoursesRepository;
import com.edu.smartedu.repository.LoginRepository;
import com.edu.smartedu.service.CoursesService;
import com.edu.smartedu.service.TrainerService;
import com.edu.smartedu.service.TypeCoursesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static com.edu.smartedu.controller.LoginController.db;

@RestController
@ComponentScan("com.edu.smartedu")
public class CoursesController {
    @Autowired
    CoursesService coursesService;
    @Autowired
    TypeCoursesService typeCoursesService;
    @Autowired
    TrainerService trainerService;

    @GetMapping("/create-course")
    public ModelAndView showCreateForm(@ModelAttribute("course") CourseUpload courses){
        if(db != null){
            ModelAndView modelAndView = new ModelAndView("/admin/create");
            modelAndView.addObject("courseUpload", new CourseUpload());
            modelAndView.addObject("course", new Courses());
            modelAndView.addObject("type", typeCoursesService.findAll());
            modelAndView.addObject("trainers", trainerService.findAll());
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView("redirect:/login");
        return modelAndView;
    }


    @PostMapping("/create-course")
    // @RequestMapping(value="/create-course" , method= RequestMethod.POST,consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ModelAndView saveBlog(@ModelAttribute("course") CourseUpload courses) throws IOException {
        if(db != null){
            MultipartFile file = courses.getFileDatas();
           // MultipartFile file = productUpload.getFile();
           // productService.upLoadFile(file);
            coursesService.upLoadFile(file);
            String name = file.getOriginalFilename();

            Courses courses1 = new Courses();
            courses1.setAvailableSeat(courses.getAvailableSeat());
            courses1.setEligibility(courses.getEligibility());
            courses1.setObjectives(courses.getObjectives());
            courses1.setFee(courses.getFee());
            courses1.setFeedback(courses.getFeedback());
            courses1.setImage(name);
            courses1.setName(courses.getName());
            courses1.setTimeBegin(courses.getTimeBegin());
            courses1.setTimeEnd(courses.getTimeEnd());
            courses1.setTrainer(courses.getTrainer());
            courses1.setTypeCourses(courses.getTypeCourses());
            coursesService.save(courses1);

            ModelAndView modelAndView = new ModelAndView("/admin/create");
            modelAndView.addObject("course", new CourseUpload());
            modelAndView.addObject("message", "New course created successfully");
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView("redirect:/login");
        return modelAndView;
    }

    @GetMapping("/admin/courses")
    public ModelAndView listCourses(){
        if(db != null){
            Iterable<Courses> courses = coursesService.findAll();
            ModelAndView modelAndView = new ModelAndView("/admin/courses");
            modelAndView.addObject("userName", db.getName());
            modelAndView.addObject("courses", courses);
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView("redirect:/login");
//        Cookie cookie = new Cookie("setUser", setUser);
//        modelAndView.addObject("cookieValue", cookie);

        return modelAndView;
    }

    @GetMapping("/delete/{id}")
    public  ModelAndView deleteCourseForm(@PathVariable Long id){
        if(db != null){
            Courses course = coursesService.findById(id);
            ModelAndView modelAndView = new ModelAndView("/admin/delete");
            modelAndView.addObject("course", course);
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView("redirect:/login");
        return modelAndView;
    }

    @PostMapping("/delete-course")
    public ModelAndView deleteBlog(@ModelAttribute("course") Courses course){
        if(db != null){
            coursesService.remove(course.getId());
            Iterable<Courses> courses1 = coursesService.findAll();
            ModelAndView modelAndView = new ModelAndView("/admin/courses");
            modelAndView.addObject("courses", courses1);
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView("redirect:/login");
        return modelAndView;
    }

    @GetMapping("/edit/{id}")
    public  ModelAndView editCourseForm(@PathVariable Long id){
        if(db != null){
            Courses course = coursesService.findById(id);

            CourseUpload courseUpload = new CourseUpload();
            courseUpload.setId(course.getId());
            courseUpload.setAvailableSeat(course.getAvailableSeat());
            courseUpload.setEligibility(course.getEligibility());
            courseUpload.setFee(course.getFee());
            courseUpload.setFeedback(course.getFeedback());
            courseUpload.setImage(course.getImage());
            courseUpload.setName(course.getName());
            courseUpload.setObjectives(course.getObjectives());
            courseUpload.setTimeBegin(course.getTimeBegin());
            courseUpload.setTimeEnd(course.getTimeEnd());
            courseUpload.setTrainer(course.getTrainer());
            courseUpload.setTypeCourses(course.getTypeCourses());

            ModelAndView modelAndView = new ModelAndView("/admin/edit");
            modelAndView.addObject("type",typeCoursesService.findAll());
            modelAndView.addObject("courseUpload", courseUpload);
            modelAndView.addObject("trainers", trainerService.findAll());
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView("redirect:/login");
            return modelAndView;
    }

    @PostMapping("/edit-course")
    public ModelAndView editCourse(@ModelAttribute("course") CourseUpload courses) throws IOException {
        if(db != null){
            MultipartFile file = courses.getFileDatas();
            coursesService.upLoadFile(file);
            String name = file.getOriginalFilename();

            Courses courses1 = new Courses();
            courses1.setId(courses.getId());
            courses1.setAvailableSeat(courses.getAvailableSeat());
            courses1.setEligibility(courses.getEligibility());
            courses1.setObjectives(courses.getObjectives());
            courses1.setFee(courses.getFee());
            courses1.setFeedback(courses.getFeedback());
            courses1.setImage(name);
            courses1.setName(courses.getName());
            courses1.setTimeBegin(courses.getTimeBegin());
            courses1.setTimeEnd(courses.getTimeEnd());
            courses1.setTrainer(courses.getTrainer());
            courses1.setTypeCourses(courses.getTypeCourses());
            coursesService.save(courses1);
            //coursesService.save(course);

            ModelAndView modelAndView = new ModelAndView("/admin/edit");
            modelAndView.addObject("courseUpload", courses);
            modelAndView.addObject("message", "Course edit successfully");
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView("redirect:/login");
        return modelAndView;
    }


    //show course details for user page
    @GetMapping("/course-details/{id}")
    public ModelAndView showCourseDetails(@PathVariable Long id){
        Courses course = coursesService.findById(id);
        ModelAndView modelAndView = new ModelAndView("/course-details");
        modelAndView.addObject("course", course);
        return modelAndView;
    }
  //home page for users
    @GetMapping("/index")
    public ModelAndView showHome(){
        ModelAndView modelAndView = new ModelAndView("/index");
        return modelAndView;
    }

    @GetMapping("/courses.html")
    public ModelAndView showCourses(){
        ModelAndView modelAndView = new ModelAndView("/courses");
        return modelAndView;
    }

    @GetMapping("/course-details.html")
    public ModelAndView showCourseDetails(){
        ModelAndView modelAndView = new ModelAndView("/course-details");
        return modelAndView;
    }

}
