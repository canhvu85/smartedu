package com.edu.smartedu.repository;

import com.edu.smartedu.model.TypeCourses;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


public interface TypeCoursesRepository extends CrudRepository<TypeCourses, Long> {
}
