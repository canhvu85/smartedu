package com.edu.smartedu.service.impl;

import com.edu.smartedu.model.Courses;
import com.edu.smartedu.repository.CoursesRepository;
import com.edu.smartedu.service.CoursesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Repository
public class CoursesServiceImpl implements CoursesService {
    @Autowired
    CoursesRepository coursesRepository;
    @Override
    public Iterable<Courses> findAll() {
        return coursesRepository.findAll();
    }

    @Override
    public Courses findById(Long id) {
        return coursesRepository.findById(id).orElse(null);
    }

//    @Override
//    public Iterable<Courses> findAllWithImage() {
//        return coursesRepository.findAllWithImage();
//    }

    @Override
    public Iterable<Courses> findPopular() {
        return coursesRepository.findPopular();
    }

    @Override
    public Iterable<Courses> findCoursesByType(Long id) {
        return coursesRepository.findCoursesByType(id);
    }

    @Override
    public void save(Courses courses) {
        coursesRepository.save(courses);
    }

    @Override
    public void remove(Long id) {
        coursesRepository.deleteById(id);
    }

    @Override
    public void upLoadFile(MultipartFile file) throws IOException {
        file.transferTo(new File("C:\\Users\\Phan Anh Vu\\Downloads\\spring-boot-learning-master\\spring-boot-learning-master\\smartedu\\src\\main\\resources\\static\\img\\" + file.getOriginalFilename()));
    }
}

