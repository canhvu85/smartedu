package com.edu.smartedu.controller;

import com.edu.smartedu.model.Trainer;
import com.edu.smartedu.model.TypeCourses;
import com.edu.smartedu.service.TrainerService;
import com.edu.smartedu.service.TypeCoursesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import static com.edu.smartedu.controller.LoginController.db;

@Controller
public class TrainerController {
    @Autowired
    TrainerService trainerService;

    @GetMapping("/create-trainer")
    public ModelAndView showCreateForm(){
        if(db != null){
            ModelAndView modelAndView = new ModelAndView("/admin/create-trainer");
            //modelAndView.addObject("typeCourse", new TypeCourses());
            modelAndView.addObject("trainer", new Trainer());
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView("redirect:/login");
        return modelAndView;
    }


    @PostMapping("/create-trainer")
    // @RequestMapping(value="/create-course" , method= RequestMethod.POST,consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ModelAndView saveTrainer(@ModelAttribute("trainer") Trainer trainer){
        //coursesService.save(courses);
        trainerService.save(trainer);
        ModelAndView modelAndView = new ModelAndView("/admin/create-trainer");
        modelAndView.addObject("trainer", new Trainer());
        modelAndView.addObject("message", "New Trainer created successfully");
        return modelAndView;
    }

    @GetMapping("/admin/trainers")
    public ModelAndView listTrainers(){
        if(db != null){
            Iterable<Trainer> trainers = trainerService.findAll();
            ModelAndView modelAndView = new ModelAndView("/admin/trainers");
            modelAndView.addObject("userName", db.getName());
            modelAndView.addObject("trainers", trainers);
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView("redirect:/login");
//        Cookie cookie = new Cookie("setUser", setUser);
//        modelAndView.addObject("cookieValue", cookie);

        return modelAndView;
    }

    @GetMapping("/delete-trainer/{id}")
    public  ModelAndView deleteTrainerForm(@PathVariable Long id){
        Trainer trainer = trainerService.findById(id);
        ModelAndView modelAndView = new ModelAndView("/admin/delete-trainer");
        modelAndView.addObject("trainer", trainer);
        return modelAndView;
    }

    @PostMapping("/delete-trainer")
    public ModelAndView deleteTrainer(@ModelAttribute("trainer") Trainer trainer){
        trainerService.remove(trainer.getId());
        Iterable<Trainer> trainers = trainerService.findAll();
        ModelAndView modelAndView = new ModelAndView("/admin/trainers");
        modelAndView.addObject("trainers", trainers);
        return modelAndView;
    }

    @GetMapping("/edit-trainer/{id}")
    public  ModelAndView editTrainerForm(@PathVariable Long id){
        Trainer trainer = trainerService.findById(id);
        ModelAndView modelAndView = new ModelAndView("/admin/edit-trainer");
        modelAndView.addObject("trainer",trainerService.findById(id));
        // modelAndView.addObject("course", course);
        return modelAndView;
    }

    @PostMapping("/edit-trainer")
    public ModelAndView editCourse(@ModelAttribute("trainer") Trainer trainer){
        trainerService.save(trainer);

        ModelAndView modelAndView = new ModelAndView("/admin/edit-trainer");
        modelAndView.addObject("trainer", trainer);
        modelAndView.addObject("message", "Trainer edit successfully");
        return modelAndView;
    }



}
