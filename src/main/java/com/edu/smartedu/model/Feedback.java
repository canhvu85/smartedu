package com.edu.smartedu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "feedbacks")
@EntityListeners(AuditingEntityListener.class)
public class Feedback {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private float quality;
    private float puncuality;
    private String content;


    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "courses_id")
    private Courses courses;

    @OneToOne
    @JsonIgnore
    @JoinColumn(name = "user_id")
    private User user;


    public Feedback() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public float getQuality() {
        return quality;
    }

    public void setQuality(float quality) {
        this.quality = quality;
    }

    public float getPuncuality() {
        return puncuality;
    }

    public void setPuncuality(float puncuality) {
        this.puncuality = puncuality;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Courses getCourses() {
        return courses;
    }

    public void setCourses(Courses courses) {
        this.courses = courses;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
