package com.edu.smartedu.service.impl;

import com.edu.smartedu.model.TypeCourses;
import com.edu.smartedu.repository.TypeCoursesRepository;
import com.edu.smartedu.service.TypeCoursesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TypeCoursesServiceImpl implements TypeCoursesService {
    @Autowired
    TypeCoursesRepository typeCoursesRepository;
    @Override
    public Iterable<TypeCourses> findAll() {
        return typeCoursesRepository.findAll();
    }

    @Override
    public TypeCourses findById(Long id) {
        return typeCoursesRepository.findById(id).orElse(null);
    }

    @Override
    public void save(TypeCourses typeCourses) {
        typeCoursesRepository.save(typeCourses);
    }

    @Override
    public void remove(Long id) {
        typeCoursesRepository.deleteById(id);
    }
}
